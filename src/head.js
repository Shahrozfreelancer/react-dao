import React, {useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

function Head(){

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

return(
    <>
        <header>
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="head-img">
                            <img src="/images/icon.png" alt="" ></img>
                            <h1>ScholarshipDAO 
                                <span>$fund</span>
                            </h1>
                        </div>
                        <p className="float-start w-100 mt-2">Decentralised  naming for wallets, website & more.</p>
                        <h4 className="bg-warning text-black text-decoration-none py-2 rounded-pill float-start px-4">Treasury $100,000 </h4>
                    </div>
                    <div className="col-lg-6">
                        
                        <Button variant="warning" className="btn btn-outline-dark px-4 py-3 float-end ms-2 btn-warning" title="">Donate</Button>
                        <Button variant="outline-dark" className="btn btn-outline-dark px-4 py-3 float-end" title="" onClick={handleShow}>Create new proposal</Button>
                        <div className="float-end text-end me-4">
                            <h3 className="mb-0">15</h3>
                            <p className="mb-0">Proposals</p>
                        </div>
                        <Modal 
                            show={show} 
                            size="lg"
                            onHide={handleClose}>
                            <Modal.Header closeButton>
                            <Modal.Title>Proposal</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <Form>
                                <Form.Group className="mb-3" controlId="amout">
                                    <Form.Label>Amounts you want to raised </Form.Label>
                                    <Form.Control className="form-control-lg"  type="number" placeholder="$999" />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="application">
                                    <Form.Label>Add link to your application</Form.Label>
                                    <Form.Control className="form-control-lg" type="text" placeholder="https://" />
                                </Form.Group>
                                <Button variant="primary" className="float-end px-4 py-2 btn btn-warning btn-lg" type="submit">
                                <img src="/images/metamask.webp" className="float-start mt-2 me-2" style={{width:16}} alt="" /> Submit
                                </Button>
                            </Form>
                            </Modal.Body>
                        </Modal>
                    </div>
                </div>
            </div>
        </header>
    </>
)}

export default Head;