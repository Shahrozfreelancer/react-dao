import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from 'react-bootstrap/Card';


function Delegate(){
return(
    <>
        <section className="float-start w-100 mt-5">
            <Card className="w-100 p-3">
                <Card.Body>
                    <Card.Title>DAO members</Card.Title>
                    <div className="float-start w-100 mt-2">
                        <div className="row">
                            <div className="col-lg-6 mb-3">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-1.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Shahroz Mushtaq</h3>
                                            <div className="ms-0">35691 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            <div className="col-lg-6 mb-3">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-2.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Fatima Rizwan</h3>
                                            <div className="ms-0">1779 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            <div className="col-lg-6 mb-3">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-3.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Pashmeena Noor</h3>
                                            <div className="ms-0">56133 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            <div className="col-lg-6 mb-3">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-4.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Usman Rashid</h3>
                                            <div className="ms-0">56461 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            <div className="col-lg-6">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-5.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Sinwan Shahid</h3>
                                            <div className="ms-0">65498 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            <div className="col-lg-6">
                                <Card className="w-100 p-3">
                                    <div className="proposal-name-sec">
                                        <img src="images/user-6.jpg" alt=""></img>
                                        <div className="proposal-name">
                                            <h3>Ahmad Ali</h3>
                                            <div className="ms-0">54653 Votes</div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                            
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </section>
    </>
)}

export default Delegate;