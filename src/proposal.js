import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Table from 'react-bootstrap/Table';
import ProgressBar  from 'react-bootstrap/ProgressBar';

function Proposals(){
return(
    <>
        <section className="float-start w-100 mt-5">
                <Table  bordered>
                    <thead>
                        <tr>
                        <th>Proposal</th>
                        <th>Votes for</th>
                        <th>Votes againt</th>
                        <th class="text-end">Total votes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr> 
                            <td>
                                <div className="proposal-name-sec">
                                    <img src="images/proposal.png" alt=""></img>
                                    <div className="proposal-name">
                                        <h3>Execute EP61</h3>
                                        <span className="btn btn-primary btn-sm py-0 px-2 float-start mt-1">EXECUTE</span><div>- ID: 82665...54865</div>
                                        <span className="btn btn-sm py-0 px-2 float-start mt-1 ms-1 btn-success">👍</span>
                                        <span className="btn btn-sm py-0 px-2 float-start mt-1 ms-1 btn-danger">👎</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-primary">2.27M</span>
                                    <ProgressBar className="mt-1" now={60} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-danger">466.8</span>
                                    <ProgressBar className="mt-1" variant="danger"  now={20} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div className="total-votes text-end">
                                    <strong>2.27M</strong><br></br>
                                    <span>104 addresses</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="proposal-name-sec">
                                    <img src="images/proposal.png" alt=""></img>
                                    <div className="proposal-name">
                                        <h3>Execute EP61</h3>
                                        <span className="badge bg-primary">EXECUTE</span><div>- ID: 82665...54865</div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-primary">2.27M</span>
                                    <ProgressBar className="mt-1" now={42} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-danger">466.8</span>
                                    <ProgressBar className="mt-1" variant="danger"  now={53} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div className="total-votes text-end">
                                    <strong>2.27M</strong><br></br>
                                    <span>104 addresses</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="proposal-name-sec">
                                    <img src="images/proposal.png" alt=""></img>
                                    <div className="proposal-name">
                                        <h3>Execute EP61</h3>
                                        <span className="badge bg-primary">EXECUTE</span><div>- ID: 82665...54865</div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-primary">2.27M</span>
                                    <ProgressBar className="mt-1" now={75} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-danger">466.8</span>
                                    <ProgressBar className="mt-1" variant="danger"  now={10} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div className="total-votes text-end">
                                    <strong>2.27M</strong><br></br>
                                    <span>104 addresses</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="proposal-name-sec">
                                    <img src="images/proposal.png" alt=""></img>
                                    <div className="proposal-name">
                                        <h3>Execute EP61</h3>
                                        <span className="badge bg-primary">EXECUTE</span><div>- ID: 82665...54865</div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-primary">2.27M</span>
                                    <ProgressBar className="mt-1" now={60} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-danger">466.8</span>
                                    <ProgressBar className="mt-1" variant="danger"  now={20} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div className="total-votes text-end">
                                    <strong>2.27M</strong><br></br>
                                    <span>104 addresses</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="proposal-name-sec">
                                    <img src="images/proposal.png" alt=""></img>
                                    <div className="proposal-name">
                                        <h3>Execute EP61</h3>
                                        <span className="badge bg-primary">EXECUTE</span><div>- ID: 82665...54865</div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-primary">2.27M</span>
                                    <ProgressBar className="mt-1" now={42} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div class="votes-for">
                                    <span className="smalll text-danger">466.8</span>
                                    <ProgressBar className="mt-1" variant="danger"  now={53} style={{height:"8px"}}/>
                                </div>    
                            </td>
                            <td>
                                <div className="total-votes text-end">
                                    <strong>2.27M</strong><br></br>
                                    <span>104 addresses</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </Table>
        </section>
    </>
)}

export default Proposals;