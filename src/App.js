
import './App.css';
import Head from './head';
import Proposals from './proposal';

function App() {
  return (
    <>
      <section>
        <Head></Head>
        <div className='float-start w-100'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-12'>
                <Proposals></Proposals>
              </div>
            </div>
          </div>
        </div>
        <footer className='border-top'>
          <span>Develope by</span>
          <img src="images/crystalball.webp" width={20} alt="" />
          <strong>Metaschool</strong>
        </footer>
        
        
      </section>
    </>
  );
}

export default App;
