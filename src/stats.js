import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

function Stats(){
return(
    <>
        <section className="float-start w-100">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="stats float-start w-100 mt-5 bg-light rounded-1 text-center p-4">
                            <div className="row">
                                
                                <div className="col">
                                    <h3 className="mb-0">15</h3>
                                    <p className="mb-0">Proposals</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
)}

export default Stats;